Linux:
	g++ -o pong src/main.cpp -std=c++17 -lX11 -lGL -lpthread -lpng

Win64_cc-gcc:
	x86_64-w64-mingw32-g++ -o pong64.exe src/main.cpp -std=c++17 -static-libstdc++ -static-libgcc -luser32 -lgdi32 -lopengl32 -lgdiplus

Win32_cc-gcc:
	i686-w64-mingw32-g++ -o pong.exe src/main.cpp -std=c++17 -static-libstdc++ -static-libgcc -luser32 -lgdi32 -lopengl32 -lgdiplus
