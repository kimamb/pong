#ifndef BALL_HPP
#define BALL_HPP

#include "paddle.hpp"

class Ball_object{
public:
  Ball_object();
  ~Ball_object();

  void hit(Paddle *paddleptr );
  void reset();

  void setx( float xpos );
  void sety( float ypos );
  void setspeedx( float speed );
  void setspeedy( float speed );
  void setdir( bool direction );

  float getx();
  float gety();
  float getspeedx();
  float getspeedy();
  bool getdir();

  int speedmod = (RESOLUTION_W / 15);
protected:
  float x;
  float y;
  float x_speed;
  float y_speed;
  bool dir;
};

Ball_object::Ball_object(){
  x = RESOLUTION_W / 2;
  y = RESOLUTION_H / 2;
  dir = 0;
}
Ball_object::~Ball_object(){

}

void Ball_object::hit(Paddle *paddleptr ){
  // Change the direction of the ball
  if(dir == 0){dir = 1;} else if(dir == 1){dir = 0;}
  // Get the padllespeed from the paddle pointer
  float paddlespeed = paddleptr->getspeed();

  // Add the speed of the paddle to the ball
  y_speed += paddlespeed;
  x_speed += paddlespeed;

  // Make the ball go the other direction.
  x_speed = -(x_speed);
}

void Ball_object::reset(){
  x = RESOLUTION_W / 2;
  y = RESOLUTION_H / 2;
  speedmod = (RESOLUTION_W / 15);
}

void Ball_object::setx( float xpos ){
  x = xpos;
}

void Ball_object::sety( float ypos ){
  y = ypos;
}

void Ball_object::setspeedx ( float speed ){
  x_speed = speed;
}

void Ball_object::setspeedy ( float speed ){
  y_speed = speed;
}

void Ball_object::setdir( bool direction ){
  dir = direction;
}

float Ball_object::getx(){
  return x;
}

float Ball_object::gety(){
  return y;
}

float Ball_object::getspeedx(){
  return x_speed;
}

float Ball_object::getspeedy(){
  return y_speed;
}

bool Ball_object::getdir(){
  return dir;
}
#endif
