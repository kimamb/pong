#ifndef PADDLE_HPP
#define PADDLE_HPP

//int RESOLUTION_H = 360/4;
//int RESOLUTION_W = 640/4;

// Define the "Paddle" class
class Paddle{
public:
  Paddle();
  ~Paddle();

  void setx( float xpos );
  void sety( float ypos );
  void setspeed( int float_speed );
  float getx();
  float gety();
  int getspeed();
protected:
  float x;
  float y;
  float speed;
};

Paddle::Paddle(){
  x = 0;
  y = (RESOLUTION_H / 2) - 11;
  speed = 0;
}

Paddle::~Paddle(){

}

void Paddle::setx(float xpos){
  x = xpos;
}

void Paddle::sety(float ypos){
  y = ypos;
}

void Paddle::setspeed(int float_speed){
  speed = float_speed;
}

float Paddle::getx(){
  return x;
}

float Paddle::gety(){
  return y;
}

int Paddle::getspeed(){
  return speed;
}
#endif
