#ifndef RENDER_HPP
#define RENDER_HPP

#include "olcPixelGameEngine.hpp"
#include "objects/paddle.hpp"
#include "objects/ball.hpp"
#include "objects/score.hpp"

#include <iostream>

#ifdef __linux__
#include "olcPGEX_Sound.hpp"
#endif


// Initialize the left and right paddle object
Paddle Leftpaddle;
Paddle Rightpaddle;

// Initialize the ball object
Ball_object Ball;

// Initialize the scores
Score Score1;
Score Score2;

// Menu
bool menu = true;

// Constants
int PADDLEMOVESPEED = (RESOLUTION_H / 5);
int BALLSIZE = (RESOLUTION_W / 200);

// Overide base class with your custom functionality
class Render : public olc::PixelGameEngine{
public:
  Render(){
    sAppName = "Pong";
  }
private:
public:
  bool OnUserCreate() override{
    // Called once at the start, so create things here

    // Set the left paddle to 0 (all the way to the left).
    Leftpaddle.setx(1);

    // Set the right paddle all the way to the right, minus one, so the paddle doesn't get drawn outside of the visible area.
    Rightpaddle.setx(RESOLUTION_W - (RESOLUTION_W / 100) - 2);

    Ball.setspeedy(0);

    return true;
  }
  bool OnUserUpdate(float fElapsedTime) override{
    if(menu == true){
      // Draw "Pong" text.
      DrawString(((RESOLUTION_W / 2) - (RESOLUTION_W / 10)), (RESOLUTION_H / 4) , "Pong",olc::WHITE,(RESOLUTION_W / 100));

      DrawString(((RESOLUTION_W / 2) - (RESOLUTION_W / 10)), (RESOLUTION_H / 2) , "Press \"Space\" \nto start!",olc::WHITE,(RESOLUTION_W / 300));

      if ((GetKey(olc::Key::SPACE).bHeld) == 1){
        menu = false;
      }
}
    if(menu == false){
    // Called once per frame:
    // Clear the screen.
    Clear(olc::Pixel(olc::BLACK));

    // Move the left paddle.
    if ((GetKey(olc::Key::W).bHeld) == 1){
      Leftpaddle.setspeed(-(PADDLEMOVESPEED));
    }
    if ((GetKey(olc::Key::S).bHeld) == 1){
      Leftpaddle.setspeed(+(PADDLEMOVESPEED));
    }
    if (( (GetKey(olc::Key::W).bHeld) == 0 ) && ( (GetKey(olc::Key::S).bHeld) == 0 )){
      Leftpaddle.setspeed(0);
    }
    // Move the right paddle
    if ((GetKey(olc::Key::UP).bHeld) == 1){
      Rightpaddle.setspeed(-(PADDLEMOVESPEED));
    }
    if ((GetKey(olc::Key::DOWN).bHeld) == 1){
      Rightpaddle.setspeed(+(PADDLEMOVESPEED));
    }
    if (( (GetKey(olc::Key::UP).bHeld) == 0 ) && ( (GetKey(olc::Key::DOWN).bHeld) == 0 )){
      Rightpaddle.setspeed(0);
    }

    // Set the momentum of the ball.
    if(Ball.getdir() == 0){
      Ball.setspeedx(Ball.speedmod);
    }
    if(Ball.getdir() == 1){
      Ball.setspeedx(-Ball.speedmod);
    }

    // Physics shit.
    Leftpaddle.sety(Leftpaddle.gety() + (Leftpaddle.getspeed() * fElapsedTime));
    Rightpaddle.sety(Rightpaddle.gety() + (Rightpaddle.getspeed() * fElapsedTime));
    Ball.setx(Ball.getx() + (Ball.getspeedx() * fElapsedTime));
    Ball.sety(Ball.gety() + (Ball.getspeedy() * fElapsedTime));

    // Check if the ball is on the left side.
    if(floor(Ball.getx()) == -1){
      Score2.number++;
      Ball.reset();
      Ball.setspeedy(0);
    }

    // Check if the ball is on the right side.
    if(floor(Ball.getx()) == RESOLUTION_W){
      Score1.number++;
      Ball.reset();
      Ball.setspeedy(0);
    }

    // Draw the left paddle.
    for (int h = 0; h <= (RESOLUTION_H / 5); h++){
      for(int b = 0; b <= (RESOLUTION_W / 200); b++){
        int y = Leftpaddle.gety() + h;
        int x = Leftpaddle.getx() + b;
        Draw(x, y, olc::Pixel(olc::WHITE));

        // Check if the ball has hit one of the pixels of the paddle
        if((floor(Ball.gety() + 0.5) == (y)) && (floor(Ball.getx() + 0.5) <= x)){
					// Send a reference to the ball.hit() function which expects a pointer to the paddle class.
          Ball.hit(&Leftpaddle);
        }
      }
    }
    // Draw the right paddle.
    for (int h = 0; h <= (RESOLUTION_H / 5); h++){
      for(int b = 0; b <= (RESOLUTION_W / 200); b++){
        int y = Rightpaddle.gety() + h;
        int x = Rightpaddle.getx() + b;
        Draw(x, y, olc::Pixel(olc::WHITE));

        // Check if the ball has hit one of the pixels of the paddle
        if((floor(Ball.gety() + 0.5) == (y-BALLSIZE)) && (floor(Ball.getx() + 0.5) >= x)){
          // Send a reference to the ball.hit() function which expects a pointer to the paddle class.
          Ball.hit(&Rightpaddle);
        }
      }
    }

    // Draw the scores:
    // Player one
    DrawString(((RESOLUTION_W / 2) - (RESOLUTION_W / 6)), 2 , std::to_string(Score1.number), olc::WHITE, (RESOLUTION_W / 200));
    // Player two
    DrawString(((RESOLUTION_W / 2) + (RESOLUTION_W / 6)), 2 , std::to_string(Score2.number), olc::WHITE, (RESOLUTION_W / 200));

		// If the ball hits the edges, change the direction the ball travels.
    if(floor(Ball.gety() + 0.5) >= (RESOLUTION_H - BALLSIZE)){
      Ball.setspeedy(-(Ball.getspeedy()));
    }
    if(floor(Ball.gety() + 0.5) <= (0)){
      Ball.setspeedy(-(Ball.getspeedy()));
    }

    // Draw the ball.
    for(int x = 0; x <= BALLSIZE; x++){
      for(int y = 0; y <= BALLSIZE; y++){
        Draw((Ball.getx()+x), (Ball.gety()+y), olc::Pixel(olc::WHITE));
        }
      }
    }
    return true;
  }
};

#endif
