int RESOLUTION_C = 1;
int RESOLUTION_H = 720 / RESOLUTION_C;
int RESOLUTION_W = 1280 / RESOLUTION_C;

#include "render.hpp"

int Gameloop(){
  Render demo;
  if (demo.Construct(RESOLUTION_W, RESOLUTION_H, RESOLUTION_C, RESOLUTION_C))
    demo.Start();
  return 0;
}
