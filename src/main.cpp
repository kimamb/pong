#define OLC_PGE_APPLICATION

#include <iostream>
//#include <string>
//#include <stdexcept>
#include <unistd.h>

#include "gameinit.hpp"


using namespace std;

int opt;
bool debug;
bool help;

int main(int argc, char* argv[]){
  while ( (opt = getopt(argc, argv, "dh")) != -1 ) {  // for each option...
        switch ( opt ) {
            case 'd':
                    debug = true;
                    cout << "debug = true" << endl;
                break;
            case 'h':
                    help = true;
                    cout << "help = true" << endl;
                break;
            case '?':  // unknown option...
                    cerr << "Unknown option: '" << char(optopt) << "'!" << endl;
                break;
        }
    }
    Gameloop();
    return 0;
}
